﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ExemploApiAutentication.Repositorio
{
    public class UserRepository
    {
        //Initialize the db context
        //private BI_JURIDICOEntities _Context;
        public UserRepository()
        {
            //Creating the db context object
            //_Context = new BI_JURIDICOEntities();
        }

        //Validate User by user name and passord
        public bool ValidateUser(string userName, string Password)
        {

            var usuario = ConfigurationManager.AppSettings["USUARIO"];
            var senha = ConfigurationManager.AppSettings["SENHA"];
            //var result = _Context.UserMaster.SingleOrDefault(x => x.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && x.Password == Password);

            return usuario.Equals(userName.TrimStart().TrimEnd()) && Password.Equals(senha.TrimStart().TrimEnd());
        }
    }
}
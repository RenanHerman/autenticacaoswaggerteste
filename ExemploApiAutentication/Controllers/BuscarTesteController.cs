﻿using ExemploApiAutentication.Dao;
using ExemploApiAutentication.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExemploApiAutentication.Controllers
{
    public class BuscarTesteController : ApiController
    {
        public List<Pessoa> Listapessoa = new List<Pessoa>();

        // GET: api/BuscarTeste
        [BasicAthentication]
        public IEnumerable<Pessoa> Get()
        {


            Pessoa pessoa = new Pessoa();
            pessoa.id = 1;
            pessoa.nome = "João";
            pessoa.cidade = "Contagem";

            Listapessoa.Add(pessoa);

            Pessoa pessoa1 = new Pessoa();
            pessoa1.id = 2;
            pessoa1.nome = "Maria";
            pessoa1.cidade = "Betim";
            Listapessoa.Add(pessoa1);

            return Listapessoa;
        }

        // GET: api/BuscarTeste/5
        [BasicAthentication]
        public string Get(int id)
        {
            return "value";
        }

        [BasicAthentication]
        // POST: api/BuscarTeste
        public void Post([FromBody]string value)
        {
        }

        [BasicAthentication]
        // PUT: api/BuscarTeste/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [BasicAthentication]
        // DELETE: api/BuscarTeste/5
        public void Delete(int id)
        {
        }
    }
}

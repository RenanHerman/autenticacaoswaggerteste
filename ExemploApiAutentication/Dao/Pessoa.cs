﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExemploApiAutentication.Dao
{
    public class Pessoa
    {   
        public int id { get; set; }
        public string nome { get; set; }
        public string cidade { get; set; }
    }
}